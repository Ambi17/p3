import java.util.ArrayList;

public class Tree {
	ArrayList <Node> nodes; // list with all nodes in the tree
	ArrayList <Edges> edges; // list with all edges of the tree
	Node root;  // is a element of nodes
	ArrayList <Node> leafs; // is a part of nodes
	int depth; // Maximale Tiefe des Baumes
	
	public Tree(ArrayList<Node> nodes) {
		super();
		this.nodes = nodes;
		generateEdges();
		generateRoot();
		generateLeafs();
		this.getDepthFirst();
	}

	private void generateEdges() {  // Bestimmt zu jedem Knoten alle Kanten zu den Kindern
		for (Node node:nodes){
			for (Node child: node.getChilds()){
				edges.add ( new Edges (node, child, "label")); // ich wei� nicht wie die Label benannt werden sollen
			}
		}
	}

	private void generateRoot() { // Generiert die Wurzel und �berpr�ft, dass es nur eine gibt
		for (Node node :nodes){
			int i= 0;
			if (node.getParent() == null){
				i +=1;
				if (i > 1){
					System.out.println("There is more than one root");
					break;
				}
				else {root = node;}
			}
		}
		
	}

	private void generateLeafs() {
		for (Node node: nodes){
			if (node.getChilds() == null){
				leafs.add(node);
			}
		}
	}

	public ArrayList<Node> getNodes() {
		return nodes;
	}

	public void setNodes(ArrayList<Node> nodes) {
		this.nodes = nodes;
		generateEdges ();
		generateRoot();
		generateLeafs();
	}

	public ArrayList<Edges> getEdges() {
		return edges;
	}

	public void setEdges(ArrayList<Edges> edges) {
		this.edges = edges;
		for (Edges edge : edges){
			Node eStart = edge.getStartNode();
			Node eEnd = edge.getEndNode();
			if (!nodes.contains(eStart)){
				System.out.println("Der Startknotenn muss sich im Baum befinden");
			}
			eEnd.setParent(eStart); // Wenn der Knoten schon existiert wird der Elternknoten �berschrieben
			eStart.addChild(eEnd);
			
			if (!nodes.contains(eEnd)){
				nodes.add(eEnd);}
		}
	}

	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) { // die alte Wurzel bekommt die neue Wurzel als Elternknoten
		this.root.setParent(root);
		this.root = root;
	}

	public ArrayList<Node> getLeafs() {
		return leafs;
	}

	public void setLeafs(ArrayList<Node> leafs) {
		this.leafs = leafs;
		
		for (Node leaf: leafs){
			leaf.setChilds(null);;
			if (!nodes.contains(leaf) && nodes.contains(leaf.getParent())){
				nodes.add(leaf);
		}
		}
		this.getDepthFirst();
	}
	
	public int getDepth() {
		return depth;
	}

	public void addLeaf (Node newLeaf){
		if (nodes.contains(newLeaf.getParent())){
			newLeaf.setChilds(null);
			leafs.add(newLeaf);
			nodes.add(newLeaf); // every leaf is also an node
			int d = depthStep(newLeaf);
			if (d > depth){
				depth = d;
			}
		}
	}
	
	public void getDepthFirst (){ 
		for (Node leaf: leafs){
			int d = depthStep(leaf);
			if  (d > depth){
				depth = d;
			}
			
		}
		
	}
	
	public int depthStep (Node l){
		int d = 0;
		while (l != root){
			d += 1;
			l = l.getParent();
		}
		if (d > this.depth){
			this.depth = d;
		}
		
		return d;
	}

}
