
public class Edges {
	Node startNode;
	Node endNode; 
	String label;
	
	public Edges(Node startNode, Node endNode, String label) {
		super();
		this.startNode = startNode;
		this.endNode = endNode;
		this.label = label;
	}

	public Node getStartNode() {
		return startNode;
	}

	public void setStartNode(Node startNode) {
		this.startNode = startNode;
	}

	public Node getEndNode() {
		return endNode;
	}

	public void setEndNode(Node endNode) {
		this.endNode = endNode;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	

}
