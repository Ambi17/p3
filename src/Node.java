import java.util.ArrayList;

public class Node {
	String header; 
	Node parent; 
	ArrayList <Node> childs;
	public Node(String header, Node parent, ArrayList<Node> childs) {
		super();
		this.header = header;
		this.parent = parent;
		this.childs = childs;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public Node getParent() {
		return parent;
	}
	public void setParent(Node parent) {
		this.parent = parent;
	}
	public ArrayList<Node> getChilds() {
		return childs;
	}
	public void setChilds(ArrayList<Node> childs) {
		this.childs = childs;
	}
	
	public void addChild (Node newChild){
		childs.add(newChild);
	}
}

