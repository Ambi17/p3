
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * The main class to handle consolecommands and to generate a Tree from a given
 * .fasta file.
 * @author David
 */
public class David_Berhardt_Carla_Moelbert_3 {

    /**
     * Main Funtion that gets called, when to program launches from console.
     * Generates a Tree for a given .fasta file and prints out the Node headers
     * and Edge labels.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*String seqString = new String();
        try{
            seqString = new String(Files.readAllBytes(Paths.get(args[0])));
        } catch (IOException ex) {
            Logger.getLogger(David_Berhardt_Carla_Moelbert_3.class.getName()).log(Level.SEVERE, null, ex);
        }
        String seq = seqString.split("\n")[1];
        seq += "$";
        WOTD test = new WOTD(seq);
        System.out.println("Headers of Nodes:");
        for (Node node : test.tree.getNodes()){
            String head = node.getHeader();
            System.out.println(head);
        }
        System.out.println("\nLabels of Edges:");
        for (Edge edge : test.tree.getEdges()){
            String label = edge.getLabel();
            System.out.println(label);
        }*/
        
        File file = new File(args[0]);
        Clustering cluster = Clustering.GetClusterFromFile(file);
        System.out.println(cluster.tree.getRoot().childHeaders.size());
    }
    
}
