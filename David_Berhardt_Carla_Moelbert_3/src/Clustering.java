import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author David
 */
public class Clustering {
    
    Tree tree;
    Tree clusteredTree;
    int[] distanceMatrix;
    int sizeSeqs;
    
    
    Clustering(ArrayList<Node> nodes){
        tree = new Tree(nodes);
        sizeSeqs = tree.getRoot().sequence.size();
    }
    
    public static Clustering GetClusterFromFile(File file){
        ArrayList<Sequence> sequences = Sequence.readMultSeqFromFile(file);
        ArrayList<Node> nodes = new ArrayList();
        Node root = new Node(new ArrayList(), null, new ArrayList(), new ArrayList());
        nodes.add(root);
        for (Sequence seq : sequences){
            ArrayList<String> childHeaders = new ArrayList(Arrays.asList(seq.seqHeader));
            ArrayList<Sequence> seqs = new ArrayList(Arrays.asList(seq));
            Node newNode = new Node(childHeaders, root, nodes, seqs);
            newNode.distance = -1;
            nodes.add(newNode);
            root.addChild(newNode);
            root.sequence.add(seq); // TODO schön machen
            root.childHeaders.add(seq.seqHeader); // TODO schön machen
        }
        
        return new Clustering(nodes);
    }
    
    private void GenerateDistanceMatrix(int size){
        distanceMatrix = new int[size];
        for (int j = 0; j < sizeSeqs; j++){
            int i = 0;
            while (i < j){
                distanceMatrix[i +  j*sizeSeqs] = HammingDist(tree.getRoot().sequence.get(j), tree.getRoot().sequence.get(i));
                i++;
            }
        }
    }
    
    private int HammingDist(Sequence seq1, Sequence seq2){
        int s = Integer.min(seq1.size, seq2.size);
        int counter = Integer.max(seq1.size, seq2.size) - Integer.min(seq1.size, seq2.size);
        for (int i = 0; i < s; i++){
            if (seq1.seq[i] != seq2.seq[i]){
                counter++;
            }
        }
        return counter;
    }
    
    public void cluster(){
        ArrayList<Node> nodesToProcess = tree.getLeafs();
        while(nodesToProcess.size() > 1){
            GenerateDistanceMatrix(nodesToProcess.size() * nodesToProcess.size());
            int min = ArrayMin(distanceMatrix);
            int j = nodesToProcess.indexOf(min);
            Node seq1 = nodesToProcess.get(j % sizeSeqs);
            Node seq2 = nodesToProcess.get(j / sizeSeqs);
            
            ArrayList<String> childHeaders = new ArrayList();
            ArrayList<Node> childNodes = new ArrayList();
            ArrayList<Sequence> sequence = new ArrayList();
            childHeaders.addAll(seq1.childHeaders);
            childHeaders.addAll(seq2.childHeaders);
            childNodes.add(seq1);
            childNodes.add(seq2);
            sequence.addAll(seq1.sequence);
            sequence.addAll(seq2.sequence);
            Node newNode = new Node(childHeaders, tree.getRoot(), childNodes, sequence);
            newNode.distance = min;
            
            nodesToProcess.remove(seq1);
            nodesToProcess.remove(seq2);
            nodesToProcess.add(newNode);
            
            seq1.setParent(newNode);
            seq2.setParent(newNode);
            
            tree.getRoot().setChilds(nodesToProcess);
        }
    }
    
    private int ArrayMin(int[] list){
        int min = list[0];
        for (int i = 1; i < list.length; i++){
            if (min > list[i]){
                min = list [i];
            }
        }
        
        return min;
    }
    
}
