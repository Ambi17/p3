import java.util.ArrayList;


/**
 * Class to Implement the structor of a Tree using Nodes an Edges between them.
 * @author Carla
 */
public class Tree {
	ArrayList <Node> nodes; // list with all nodes in the tree
	ArrayList <Edge> edges; // list with all edges of the tree
	Node root;  // is a element of nodes
	ArrayList <Node> leafs; // is a part of nodes
	int depth; // Maximale Tiefe des Baumes
        Mode mode;
        
        
        enum Mode{cluster, suffix};
        
        
	
        /**
         * Constructor to create a Tree from an ArrayList of Nodes.
         * @param nodes Nodes to create Tree from
         */
	public Tree(ArrayList<Node> nodes) {
		this.nodes = nodes;
                edges = new ArrayList();
                leafs = new ArrayList();
		generateEdges();
		generateRoot();
		generateLeafs();
		this.getDepthFirst();
	}

    public void setMode(Mode mode) {
        this.mode = mode;
    }

        /**
         * Generates the Edges for a given Tree.
         */
	private void generateEdges() {  // Bestimmt zu jedem Knoten alle Kanten zu den Kindern
		for (Node node:nodes){
			for (Node child: node.getChilds()){
				edges.add ( new Edge (node, child, "label")); // ich wei� nicht wie die Label benannt werden sollen
			}
		}
	}

        /**
         * Findes the root of a Tree.
         */
	private void generateRoot() { // Generiert die Wurzel und �berpr�ft, dass es nur eine gibt
		for (Node node :nodes){
			int i= 0;
			if (node.getParent() == null){
				i +=1;
				if (i > 1){
					System.out.println("There is more than one root");
					break;
				}
				else {root = node;}
			}
		}
		
	}

        /**
         * Findes all Leafs of a Tree.
         */
	private void generateLeafs() {
		for (Node node: nodes){
			if (node.getChilds().isEmpty()){
				leafs.add(node);
			}
		}
	}

        /**
         * Gets all Nodes of a given Tree.
         * @return This Trees Nodes.
         */
	public ArrayList<Node> getNodes() {
		return nodes;
	}

        /**
         * Sets new Nodes for a given Tree.
         * @param nodes new Nodes
         */
	public void setNodes(ArrayList<Node> nodes) {
		this.nodes = nodes;
		generateEdges ();
		generateRoot();
		generateLeafs();
	}

        /**
         * Gets all Edges of a given Tree.
         * @return This Trees Edges
         */
	public ArrayList<Edge> getEdges() {
		return edges;
	}

        /**
         * Sets all Edges of a given Tree.
         * @param edges new Edges
         */
	public void setEdges(ArrayList<Edge> edges) {
		this.edges = edges;
	}

        /**
         * Gets the root of a given Tree.
         * @return This Trees root
         */
	public Node getRoot() {
		return root;
	}

        /**
         * Sets a new root for a given Tree.
         * @param root new root
         */
	public void setRoot(Node root) { // die alte Wurzel bekommt die neue Wurzel als Elternknoten
		this.root.setParent(root);
		this.root = root;
	}

        /**
         * Gets all Leafs of a given Tree.
         * @return This Trees Leafs
         */
	public ArrayList<Node> getLeafs() {
		return leafs;
	}

        /**
         * Sets all Leafs for a given Tree.
         * @param leafs new Leafs
         */
	public void setLeafs(ArrayList<Node> leafs) {
		this.leafs = leafs;
		
		for (Node leaf: leafs){
			leaf.setChilds(null);;
			if (!nodes.contains(leaf) && nodes.contains(leaf.getParent())){
				nodes.add(leaf);
		}
		}
		this.getDepthFirst();
	}
	
        /**
         * Gets the depth of a given Tree.
         * @return This Trees Depth
         */
	public int getDepth() {
		return depth;
	}
        
        /**
         * Adds an Edge to a goven Tree.
         * @param startNode Edges start Node
         * @param endNode Edges end Node
         * @param label Edges label
         */
        public void addEdge(Node startNode, Node endNode, String label){
            edges.add(new Edge(startNode, endNode, label));
        }

        /**
         * Adds a new Leaf to a given Tree
         * @param newLeaf new Leaf
         * @param edgeLabel Edges label to the new Leaf
         */
	public void addLeaf (Node newLeaf, String edgeLabel){
		if (nodes.contains(newLeaf.getParent())){
			newLeaf.setChilds(new ArrayList());
			leafs.add(newLeaf);
			nodes.add(newLeaf); // every leaf is also an node
                        addEdge(newLeaf.getParent(), newLeaf, edgeLabel);
                        leafs.remove(newLeaf.getParent());
			int d = depthStep(newLeaf);
			if (d > depth){
				depth = d;
			}
		}
	}
	
        /**
         * Sets the depth for a Tree.
         */
	public void getDepthFirst (){ 
		for (Node leaf: leafs){
			int d = depthStep(leaf);
			if  (d > depth){
				depth = d;
			}
			
		}
		
	}
	
        /**
         * Gets the depth of a given Node.
         * @param l Node to get depth for
         * @return The depth of the given Node
         */
	public int depthStep (Node l){
		int d = 0;
		while (l != root){
			d += 1;
			l = l.getParent();
		}
		if (d > this.depth){
			this.depth = d;
		}
		
		return d;
	}

}
