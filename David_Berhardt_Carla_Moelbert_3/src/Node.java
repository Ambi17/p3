import java.util.ArrayList;

/**
 * A class to implement Nodes used in a Tree.
 * @author Carla
 */
public class Node {
	String header;
        ArrayList<String> childHeaders;
	Node parent; 
	ArrayList <Node> childs;
        ArrayList<Sequence> sequence;
        int distance;
        
        
        /**
         * Constructor to construct an object of type Node.
         * Used for mode suffix
         * @param header Nodes header
         * @param parent Nodes parent
         * @param childs Nodes children
         */
	public Node(String header, Node parent, ArrayList<Node> childs) {
		this.header = header;
		this.parent = parent;
		this.childs = childs;
	}
        
        /**
         * Constructor to construct an object of type Node.
         * Used for mode cluster
         * @param childHeaders headers of all childs
         * @param parent Nodes parent
         * @param childs Nodes children
         * @param sequence sequences of all childs
         */
        public Node(ArrayList<String> childHeaders, Node parent, ArrayList<Node> childs, ArrayList<Sequence> sequence){
            this.childHeaders = childHeaders;
            this.parent = parent;
            this.childs = childs;
            this.sequence = sequence;
        }
        
        /**
         * Gets the header of a given Node.
         * @return This Nodes header
         */
	public String getHeader() {
		return header;
	}
        
        /**
         * Sets the header of a given Node.
         * @param header new header
         */
	public void setHeader(String header) {
		this.header = header;
	}
        
        /**
         * Gets the parent of a given Node.
         * @return This Nodes parent
         */
	public Node getParent() {
		return parent;
	}
        
        /**
         * Sets the parent for a given Node.
         * @param parent new parent
         */
	public void setParent(Node parent) {
		this.parent = parent;
	}
        
        /**
         * Gets the Childes of a give Node.
         * @return This Nodes Childes
         */
	public ArrayList<Node> getChilds() {
		return childs;
	}
        
        /**
         * Sets the Childs for a given Node.
         * @param childs new Childes
         */
	public void setChilds(ArrayList<Node> childs) {
		this.childs = childs;
	}
	
        /**
         * Adds a new Child to a given Node.
         * @param newChild new Child 
         */
	public void addChild (Node newChild){
		childs.add(newChild);
	}
}

