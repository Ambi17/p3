
/**
 * A class to implements edges between Nodes of a Tree.
 * @author Carla
 */

public class Edge {
	Node startNode;
	Node endNode; 
	String label;
	
        
        /**
         * The constructor to construct an object of type Edge.
         * @param startNode the edges start Node
         * @param endNode the edges end Node
         * @param label the edges label
         */
	public Edge(Node startNode, Node endNode, String label) {
		this.startNode = startNode;
		this.endNode = endNode;
		this.label = label;
	}

        /**
         * Returns the start Node of a given Edge.
         * @return start Node of this Edge
         */
	public Node getStartNode() {
		return startNode;
	}

        /**
         * Sets a new start Node for a given Edge:
         * @param startNode new start Node
         */
	public void setStartNode(Node startNode) {
		this.startNode = startNode;
	}

        /**
         * Gets the end Node of a given Edge.
         * @return end Node of this edge
         */
	public Node getEndNode() {
		return endNode;
	}

        /**
         * Sets the end Node for a given Edge.
         * @param endNode new end Node
         */
	public void setEndNode(Node endNode) {
		this.endNode = endNode;
	}

        /**
         * Gets the label of a given edge.
         * @return This Edges label.
         */
	public String getLabel() {
		return label;
	}

        /**
         * Sets the label of a given Edge.
         * @param label new label
         */
	public void setLabel(String label) {
		this.label = label;
	}
	
	

}
