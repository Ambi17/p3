/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A Class to represent Sequences
 * @author Carla
 */
public class Sequence {
    protected String seqHeader;
    protected String seqText;
    public String path;
    public String sourceText;
    public char[] seq;
    public ArrayList<Character> alphabeth;
    public int size;
    public int sizeA;
   
    /**
     * Constructor to create a new Sequenceobject
     * @param Text the whole .fasta file as string
     */
    public Sequence (String Text){
        String[] parts = Text.split("\n", 2);
        parts[0] = parts[0].replaceAll("\n", "");
        parts[1] = parts[1].replaceAll("\n", "");
        sourceText = Text;
        seqHeader = parts[0];
        seqText = parts[1];
        seq = seqText.toCharArray();
        char[] alp = new char[4];
        alp[2] = 'A';
        alp[3] = 'T';
        alp[0] = 'G';
        alp[1] = 'C';
        alphabeth = new ArrayList();
        setAlphabeth();
        size = seq.length;
        sizeA = alphabeth.size();
    }
    
    private void setAlphabeth() {
        for (char ch : seq){
            if (!alphabeth.contains(ch)){
                alphabeth.add(ch);
            }
        }
    }
    
    /**
     * returns an integer array to use for hashing with rabin carb
     * for a given Sequenceobject
     * @return Array of integers
     */
    public int[] charToInt (){
        int[] intArray = new int[size];
        for(int i = 0; i < size; i++){
            for (int j=0; j<sizeA; j++){
                if (seq[i] == alphabeth.get(j)){
                    intArray [i] = j;
                    break;
                }
            }
        }
        return (intArray);
    }
    
    public Sequence readSeqFromFile(File file){
        try{
            String seqText = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
        } catch (IOException ex) {
            Logger.getLogger(Sequence.class.getName()).log(Level.SEVERE, null, ex);
        }
        Sequence seq = new Sequence(seqText);
        return seq;
    }
    
    public static ArrayList<Sequence> readMultSeqFromFile(File file){
        ArrayList<Sequence> seqs = new ArrayList<Sequence>();
        String seqString = new String();
        try{
            seqString = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
        } catch (IOException ex) {
            Logger.getLogger(Sequence.class.getName()).log(Level.SEVERE, null, ex);
        }
        String[] seqsString = seqString.split(">");
        for(int i = 1; i < seqsString.length; i++){
            seqsString[i] = ">" + seqsString[i];
            Sequence s = new Sequence(seqsString[i]);
            s.path = file.getAbsolutePath();
            seqs.add(s);
        }
        return seqs;
    }
}
