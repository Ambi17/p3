
import java.util.ArrayList;
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * A class to implement the WOTD-algorithm to create a Tree from a given
 * Sequence string.
 * @author David
 */
public class WOTD {
    
    Tree tree;
    String word;
    ArrayList<String> S;
    ArrayList<Character> alphabeth;
    Node root;
    
    /**
     * Constructor to create a Tree using the WOTD algorithm.
     * @param t Sequence as string to create the Tree from
     */
    WOTD(String t){
        word = t;
        alphabeth = new ArrayList();
        S = new ArrayList();
        setAlphabeth();
        generateS();
        root = new Node("_root_", null, new ArrayList());
        tree = new Tree(new ArrayList(Arrays.asList(root)));
        tree.setMode(Tree.Mode.suffix);
        WOTDRec(S, root);
    }

    /**
     * Sets the alphabeth used for a given string.
     */
    private void setAlphabeth() {
        for (char ch : word.toCharArray()){
            if (!alphabeth.contains(ch)){
                alphabeth.add(ch);
            }
        }
    }
    
    /**
     * Generates the first set of words S
     */
    private void generateS(){
        for (int i = 0; i < word.length(); i++){
            S.add(word.substring(i, word.length()));
        }
    }
    
    /**
     * Generates a Tree for a Set of words and a given root Node.
     * @param S Set of words
     * @param r root
     */
    private void WOTDRec(ArrayList<String> S, Node r){
        for (char ch : alphabeth){
            ArrayList<String> newS = new ArrayList();
            for (String s : S){
                if (s.startsWith(Character.toString(ch))){
                    newS.add(s);
                }
            }
            if (newS.size() == 1){
                String header = r.header + newS.get(0);
                Node newLeaf = new Node(header, r, new ArrayList());
                r.addChild(newLeaf);
                tree.addLeaf(newLeaf, newS.get(0));
            }
            else if (newS.size() > 1){
                String p = longestPrefix(newS);
                String header = r.header + p;
                Node newLeaf = new Node(header, r, new ArrayList());
                r.addChild(newLeaf);
                tree.addLeaf(newLeaf, p);
                WOTDRec(suffixOfArrayList(newS, p.length()), newLeaf);
            }
        }
    }
    
    /**
     * Finds the longest common prefix of a set of words.
     * @param S Set of words
     * @return The longest common prefix
     */
    private String longestPrefix(ArrayList<String> S){
        String shortest = S.get(0);
        
        for (String str : S){
            if (shortest.length() > str.length()){
                shortest = str;
            }
        }
        
        for (int i = 1; i <= shortest.length(); i++){
            String pre = S.get(0).substring(0, i);
            for (String str : S){
                if (!str.startsWith(pre)){
                    return pre.substring(0, pre.length()-1);
                }
            }
        }
        return shortest;
    }
    
    /**
     * Cuts every word in a Set by a given amout from the start of the word.
     * @param S Set of words
     * @param prefixLength length to cut of.
     * @return 
     */
    private ArrayList<String> suffixOfArrayList(ArrayList<String> S, int prefixLength){
        ArrayList<String> newS = new ArrayList();
        
        for (String str : S){
        newS.add(str.substring(prefixLength, str.length()));
    }
        return newS;
    }
}
